Tetris clone made to get acquainted with various Phaser functionalities. Tetris' gameplay doesn't necessarily call for
using a full-fledged game engine, but I tried to cram in as many relevant functionalities as I could for practice,
and all that combined might make for an interesting example for other Phaser users. It includes:

        Changing the controls of the game
        Sound effects and music, which can be turned on/off using the relevant button, and are interrupted on the pause screen
        Tweens to animate the clearing and collapsing of lines
        A loop to manage the fall of the tetromino, which accelerates as you complete more lines
        A leaderboard served by a PHP back end to keep track of the scores of the players
        Text using bitmapfonts
        Plus the necessary basics such as imput management and rendering the sprites

Not much in terms of fancy sprites stuff and collisions unfortunately, because the game didn't really call for it.
I didn't try to polish the appearance, as it was before all an exercise in Phaser.

Most of my assets are stolen from the first results of some google searches ; if you see something that you created,
let me know and I can give you credit for it here.

=======
Credits:
=======

Start button sprite stolen from the Monster Wants Candy demo : https://github.com/EnclaveGames/Monster-Wants-Candy-demo/